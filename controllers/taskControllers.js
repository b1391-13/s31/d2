const Task = require(`./../models/Task`)


module.exports.createTask = (reqBody) => {

    let newTask = new Task({
        name: reqBody.name
    })
    return newTask.save().then(result => result)
}

module.exports.getAllTasks = () => {

    return Task.find().then((result) => {
        return result
    })
}

module.exports.getById = (params) => {
    return Task.findById(params).then(result => result)
}


module.exports.updateTask = (params) => {
    let updatedAdminStatus = {
        status: "complete"
    }
    return Task.findOneAndUpdate(params, updatedAdminStatus, {new: true}).then(result => {
        return result
    })
}