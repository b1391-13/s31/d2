const User = require('./../models/User')


//Retrive all documents
module.exports.getAllUser = () => {

    return User.find().then((result) => {
        return result
    })
}


//register a new user
module.exports.register = (reqBody) => {
    return User.findOne({email: reqBody.email}).then((result) => {
        console.log(result)
        if(result != null) {
            return `User already exist`
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                password: reqBody.password
            })
            return newUser.save().then((result) => {
                return `User saved!`
            })
        }
    })
}

//update one document
module.exports.updateUser = (email) => {
    let updatedAdminStatus = {
        isAdmin: true
    }
    return User.findOneAndUpdate({email: email}, updatedAdminStatus, {new: true}).then(result => {
        return result
    })
}


//delete documents
module.exports.deleteUser = (email) => {

    return User.findOneAndDelete({email: email}).then(result => {
        return true
    })
}

//findOne
module.exports.getSpecificUser = (email) => {
    return User.findOne({email: email}).then(result => result)
}

//findById

module.exports.getById = (params) => {
    return User.findById(params).then(result => result)
}