
//import express module
const express = require('express');
const app = express();
const PORT = process.env.PORT || 4000;

const userRoutes = require("./routes/userRoutes");
const taskRoutes = require('./routes/taskRoutes');

//import mongoose module
const mongoose = require('mongoose');

//mongodb connection
mongoose.connect('mongodb+srv://jrc4827:admin@batch139.glw5n.mongodb.net/s31?retryWrites=true&w=majority',
{useNewUrlParser: true});

//mongodb notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log(`Connected to Database`)
});

//Middleware

//helps express to understand json payload
app.use(express.json());
//json payloads come from forms and tables
app.use(express.urlencoded({extended: true}))

//middleware
app.use('/api/users', userRoutes);
app.use('/api/tasks', taskRoutes)

app.listen(PORT, () => console.log(`Server running at port ${PORT}`))