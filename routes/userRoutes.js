
const express = require("express");
const router = express.Router();

const userController = require('./../controllers/userControllers')

router.get('/', (req, res) => {
    userController.getAllUser().then(result => {
        res.send(result)
    })
});

router.post('/add-user', (req, res) => {
    //console.log(req.body);

    userController.register(req.body).then((result) => res.send(result))
})

router.put('/update-user', (req, res) => {

    userController.updateUser(req.body.email).then((result) => res.send(result))
})

router.delete('/delete-user', (req, res) => {
    userController.deleteUser(req.body.email).then((result) => res.send(result))
})

router.get('/specific-user', (req, res) => {

    userController.getSpecificUser(req.body.email).then(result => res.send(result))
})

router.get('/:id', (req,res) => {

    userController.getById(req.params.id).then(result => res.send(result))
    //console.log(req)
})




module.exports = router;